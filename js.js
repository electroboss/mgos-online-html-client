var url;
var token;

var board;
var boardsize;
var boardid;
var playerno;
var win_condition;
var started;
var dark_mode = true;
const LIGHT_COLOR = "#ffffdd";
const DARK_COLOR = "#222222";

var socket_open = false;
var moved = true;

var cell_size = get_screen_scale();

function start() {
  url = document.getElementById("url_input").value;
  playerno = Number(document.getElementById("playerno_input").value);
  var newboard = document.getElementById("newboard_input").checked;
  boardid = document.getElementById("boardid_input").value;             // These get overwritten after get_board().
  boardsize = Number(document.getElementById("boardsize_input").value); // These get overwritten after get_board().
  var passcode = document.getElementById("passcode_input").value;
  win_condition = Number(document.getElementById("win_condition_input").value);
  if (newboard) {
    request_new_board(boardid, boardsize, passcode, win_condition);
  } else {
    get_board(boardid);
  }
}

function send_data(url, data) {
  var json_text = JSON.stringify(data);
  request = new Request(url, {"body": json_text, "method": "POST", "mode":"cors"})
  fetch(request).then(
    (response) => {
      if (!response.ok) {
        document.getElementById("error_div").innerText = "Error: response not okay" + get_time();
        return;
      }

      response.json().then(handle_message).then(
        (output) => {
          if (output[0]) {
            document.getElementById("status_div").innerText = "Status: " + output[1] + get_time();
          } else {
            document.getElementById("error_div").innerText = "Error: " + output[1] + get_time();
          }
        }
      );
    }
  )
}
function handle_message(data) {
  if (data["successful"] == false) {
    return [false,data["error"]];
  }
  if (data["type"] == undefined) {
    return [false,"invalid response."];
  }
  if (data["type"] == "get_board") {
    boardsize = data["board"]["size"];
    board = data["board"]["number_array"];
    win_condition = data["board"]["win_condition"];
    started = true;
    cell_size = get_screen_scale();
    generate_board();
    get_board_state();
  }
  if (data["type"] == "start_board") {
    get_board();
  }
  if (data["type"] == "get_board_data") {
    update_board_state(data["board"]["claim_array"], data["board"]["turn"], data["board"]["factor"], data["winner"]);

    setTimeout(get_board_state, 500);
  }
  return [true,data["type"]];
}
function get_board() {
  send_data(url+"get_board", {
    "boardid": boardid
  })
}
function get_board_state() {
  send_data(url+"get_board_data", {
    "boardid": boardid
  })
}
function send_move(i,j) {
  send_data(url+"move", {
    "move": [i,j],
    "player": playerno,
    "boardid": boardid
  })
}
function request_new_board(boardid, boardsize, passcode, win_condition) {
  send_data(url+"start_board", {
    "boardid": boardid,
    "size": boardsize,
    "passcode": passcode,
    "win_condition": win_condition
  })
}

function new_board_checked() {
  var checked = document.getElementById("newboard_input").checked;
  Array.from(document.getElementsByClassName("new_board_settings")).forEach((elem) => {elem.style.display = checked ? "block" : "none";});
}

function generate_board() {
  if (!started) {
    return;
  }
  var canvas = document.getElementById("board_canvas");
  canvas.width = boardsize*cell_size;
  canvas.height = boardsize*cell_size;
  canvas.onclick = (event) => canvas_clicked(canvas, event);

  var ctx = canvas.getContext("2d");
  // 2.3 rem and 1.3 rem for 50 cell size.
  // convert to px and scale with screen size:
  // For me, 1rem = 17px.
  // 2.3 and 1.3 with this ratio → 39.1px, 22.1px.
  // These are for 50, so to make it scale with the actual amount, divide by 50.
  // → 0.782, 0.442
  const digit_font2 = (0.782*cell_size).toString()+"px \"Noto Sans\"";
  const offset2 = 0.24*cell_size;
  const digit_font3 = (0.442*cell_size).toString()+"px \"Noto Sans\"";
  const offset3 = 0.36*cell_size;
  ctx.fillStyle = dark_mode ? LIGHT_COLOR : DARK_COLOR;

  // numbers
  for (let i=0; i<boardsize; i++) {
    for (let j=0; j<boardsize; j++) {
      if (board[j][i].toString().length > 2) {
        ctx.font = digit_font3;
        var offset = offset3;
      } else {
        ctx.font = digit_font2;
        var offset = offset2;
      }
      let text_size_pix = ctx.measureText(board[j][i].toString()); // for centering
      ctx.fillText(board[j][i].toString(), i*cell_size+(cell_size-text_size_pix.width)/2, (j+1)*cell_size-offset);
    }
  }

  ctx.strokeStyle = dark_mode ? LIGHT_COLOR : DARK_COLOR;
  ctx.lineWidth = 0.05*cell_size;
  // border lines
  for (let i=1; i<boardsize; i++) {
    ctx.beginPath();
    ctx.moveTo(i*cell_size, 0);
    ctx.lineTo(i*cell_size, cell_size*boardsize);
    ctx.stroke();
  }
  for (let i=1; i<boardsize; i++) {
    ctx.beginPath();
    ctx.moveTo(0, i*cell_size);
    ctx.lineTo(cell_size*boardsize, i*cell_size);
    ctx.stroke();
  }
  canvas.style.display = "inherit";

  boardid_div = document.getElementById("boardid_div");
  boardid_div.innerText = "Board id: "+boardid;

  win_condition_div = document.getElementById("win_condition_div");
  win_condition_div.innerText = "Win condition: "+win_condition.toString();
  Array.from(document.getElementsByClassName("game_info")).forEach((elem) => {elem.style.display = "block"});
  Array.from(document.getElementsByClassName("setup"    )).forEach((elem) => {elem.style.display = "none"});

  setTimeout(auto_update_screen_scale, 3000); // Does what it says on the tin. Start here because otherwise the variables aren't initialised yet.
}

function update_board_state(boardstate, turn, factor, winner) {
  var canvas = document.getElementById("board_canvas");
  var ctx = canvas.getContext("2d");

  if (winner) {
    // see logic for other board update thing
    ctx.font = (1.7*cell_size).toString()+"px \"Noto Sans\"";
    ctx.fillText("Player "+(winner-1).toString()+" won!", 0, 2*cell_size, cell_size*boardsize);
  }
  ctx.strokeStyle = dark_mode ? LIGHT_COLOR : DARK_COLOR;
  ctx.lineWidth = 0.05*cell_size;
  for (var i=0; i<boardsize; i++) {
    for (var j=0; j<boardsize; j++) {
      switch (boardstate[j][i]) {
        case 0: // X
          ctx.moveTo(i*cell_size, j*cell_size);
          ctx.lineTo((i+1)*cell_size, (j+1)*cell_size);
          ctx.stroke();
          ctx.moveTo((i+1)*cell_size, j*cell_size);
          ctx.lineTo(i*cell_size, (j+1)*cell_size);
          ctx.stroke();
          break;
        case 1: // O
          ctx.beginPath();
          ctx.arc((i+0.5)*cell_size,(j+0.5)*cell_size, cell_size/2, 0, 2*Math.PI);
          ctx.stroke();
          break;
      }
    }
  }

  document.getElementById("turn_div").innerText = turn == playerno ? "Your turn" : "Opponent's turn";
  document.getElementById("factor_div").innerText = "Factor: " + factor.toString();
}

function canvas_clicked(canvas, event) {
  var pos = get_cursor_position(canvas, event);
  moved = true;
  send_move(Math.floor(pos[0]/cell_size), Math.floor(pos[1]/cell_size));
}

// https://stackoverflow.com/questions/55677/how-do-i-get-the-coordinates-of-a-mouse-click-on-a-canvas-element
function get_cursor_position(canvas, event) {
  const rect = canvas.getBoundingClientRect()
  const x = event.clientX - rect.left
  const y = event.clientY - rect.top
  return [x,y]
}

function get_time() {
  var currenttime = new Date();
  return " "+currenttime.toLocaleTimeString();
}

function get_screen_scale() {
  var max_size = Math.min(...[window.screen.width, window.screen.height]);
  var good_size = 0.9 * max_size; // TODO: change so there's an empty border
  var cell_size = (good_size / boardsize);

  return cell_size;
}

function auto_update_screen_scale() {
  var new_scale = get_screen_scale();
  if (new_scale != cell_size) {
    cell_size = new_scale;
    generate_board();
    // boardstate updates automatically
  }
  setTimeout(auto_update_screen_scale, 3000); // update again 3 seconds later.
}

function dark_mode_update() {
  dark_mode = document.getElementById("dark_mode_checkbox").checked;
  if (dark_mode) {
    document.body.style.background = DARK_COLOR;
    document.body.style.color = LIGHT_COLOR;
  } else {
    document.body.style.background = LIGHT_COLOR;
    document.body.style.color = DARK_COLOR;
  }
  generate_board();
}